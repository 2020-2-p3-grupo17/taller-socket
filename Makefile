CC=gcc -Wall -c
all:bin/servidor bin/cliente

bin/servidor:  obj/servidor.o
	   mkdir -p bin/
	   gcc $^ -o bin/servidor
	   
bin/cliente:  obj/cliente.o
	   gcc $^ -o bin/cliente

obj/servidor.o: src/servidor.c
		mkdir -p obj/
		$(CC) src/servidor.c -o obj/servidor.o
		
obj/cliente.o: src/cliente.c
		$(CC) src/cliente.c -o obj/cliente.o

.Phony: clean
clean: 
	rm obj/* bin/*
