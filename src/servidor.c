#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc,char **argv){
	if(argc<5){
		printf("Faltan argumentos\n");
		exit(-1);
	}
	
	char *ip;
	unsigned int port;
	char opcion;
	while((opcion = getopt(argc, argv, "i:p:")) != -1 ){
   		switch(opcion){
   			case 'i':
   				ip = optarg;
   				break;
   			case 'p':
        		if (atoi(optarg)<0){
          			printf("El puerto que ha ingresado es incorrecto\n");
        	  	exit(-1);
        		}
        		port = (unsigned int)atoi(optarg);
   				break;
   			default:
   				break;
   		}
  	}

	int so = socket(AF_INET,SOCK_STREAM,0);

	if(so<0){
		perror("Existe algun error en el socket");
		exit(-1);
	}
	
	struct sockaddr_in d;
	d.sin_family = AF_INET;
	d.sin_port = htons(port);
	d.sin_addr.s_addr =inet_addr(ip);

	int bindd = bind(so,(struct sockaddr *)&d,sizeof(d));


	if(bindd<0){
		perror("hay errores en el bind");
		close(so);
		exit(-1);
	}

	int lst = listen(so,3);


	if(lst<0){
		perror("Error en el listen");
		close(so);
		exit(-1);
	}

	while(1){
				
		int ac = accept(so,NULL,0);
		
		if(ac<0){
			perror("Error: accept");
			close(so);
			exit(-1);
	 	}
	

		char buf[1000]={0};
		int b = recv(ac,buf,1000,0);
		if(b<0){
			perror("Error en el recv");
			close(ac);
			exit(-1);
		}	
		

		int abrir =open(buf,O_RDONLY);
		if(abrir<0){
			perror("Error en el open");
			char *msj="Error";
			int s = send(ac,msj,strlen(msj),0);
			if(s<0){
				perror("Error en el send");
				close(ac);
				exit(-1);
			}
			close(ac);
		}else{
			char buffer[1000]={0};
			int ble=0;
				while((ble = read(abrir,buffer,1000))!=0){
					if(ble<0){
						perror("Error en el read");
						char *men="Error";
						int snd = send(ac,men,strlen(men),0);
						if(snd<0){
							perror("Error en el send");
							close(ac);
							exit(-1);			
						}
						close(ac);
					}else if(ble){
						int s = send (ac,buffer,ble,0);
						if(s<0){
							perror("Error en el send");
							close(ac);
							exit(-1);
						}
					}
				}

				close(abrir);
				close(ac);
		}
	}
	close(so);

	return 0;

}
					


