#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc,char **argv){
	if(argc<9){
		printf("Faltan argumentos\n"); 
		exit(-1);
	}

	char *ipC;
	unsigned int portC;
	char *ruta;
	char *name;
	char opcion;
	while((opcion = getopt(argc, argv, "i:p:R:L:")) != -1 ){
		switch(opcion){
			case 'i':
				ipC = optarg;
				break;
			case 'p':
				if (atoi(optarg)<0){
          			printf("El puerto que ha ingresado es incorrecto\n");
        	  	exit(-1);
        		}
        		portC = (unsigned int)atoi(optarg);
        		break;
        	case 'R':
        		ruta = optarg;
        		break;
        	case 'L':
        		name = optarg;
        		break;
        	default:
        		break;
		}
	}

	int soC=socket(AF_INET,SOCK_STREAM,0);

	if(soC<0){
		perror("Ha ocurrido un error en el socket");
		exit(-1);
	}

	struct sockaddr_in direccion;
	direccion.sin_family=AF_INET;
	direccion.sin_port=htons(portC);
	direccion.sin_addr.s_addr=inet_addr(ipC);

	int cnt=connect(soC,(struct sockaddr*)&direccion,sizeof(direccion));

	if(cnt<0){
		perror("Ha ocurrido un error en el connect");
		close(soC);
		exit(-1);
	}

	int snd=send(soC,ruta,strlen(ruta),0);

	if(snd<0){
		perror("Ha ocurrido un error en el send");
		close(soC);
		exit(-1);
	}


	int op=open(name,O_WRONLY|O_CREAT|O_TRUNC,0666);

	if(op<0){
		perror("Ha ocurrido un error en el open");
		close(soC);
		exit(-1);
	}

	char buf[1000]={0};
	int bR=0;
	
	while((bR=recv(soC,buf,1000,0))!=0){
		if(bR<0){
			perror("Ha ocurrido un error en el recv");
			close(soC);
			exit(-1);
		}else if(strcmp(buf,"Ha ocurrido un error")!=0 && bR>0){
			int wr=write(op,buf,bR);
			if(wr<0){
				perror("Ha ocurrido un error en el write");
				close(soC);
				exit(-1);
			}
		}else if(strcmp(buf,"Error")==0){
			printf("Ha ocurrido un error en el Servidor");
	       		close(soC);
		 	exit(-1);
		}	
	}

	close(soC);

	return 0;
}
